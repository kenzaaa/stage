defmodule PrezanceWeb.Dataset.PptImport do

    require Logger
    require Math
    alias Prezance.Documents.Element

    @defaultPPTShapeColor "#4472c4"
    @p "{http://schemas.openxmlformats.org/presentationml/2006/main}"
    @a "{http://schemas.openxmlformats.org/drawingml/2006/main}"
    @c "{http://schemas.openxmlformats.org/drawingml/2006/chart}"
    @r "{http://schemas.openxmlformats.org/officeDocument/2006/relationships}"

    def mediaElementsPic(every_pic) do
        name = every_pic["#{@p}nvPicPr"]["#{@p}cNvPr"]["-name"]
        # pptId = every_pic["#{@p}nvPicPr"]["#{@p}cNvPr"]["-id"]
        desc = every_pic["#{@p}nvPicPr"]["#{@p}cNvPr"]["-descr"]
        xfrm = every_pic["#{@p}spPr"]["#{@a}xfrm"]
        rtString = xfrm["-rot"] #16200000
        dim = dimensions(xfrm, rtString)
        xS = xfrm["#{@a}off"]["-x"]
        yS = xfrm["#{@a}off"]["-y"]
        widthS = xfrm["#{@a}ext"]["-cx"]
        heightS = xfrm["#{@a}ext"]["-cy"]
        # Units conversions
        width = toPrzDimension(dim.width)
        height = toPrzDimension(dim.height)
        x = toPrzDimension(dim.x)
        y = toPrzDimension(dim.y)
        rt = toPrzRt(rtString)
        shapeType = toPrzType(nil, every_pic, nil)
        title =
          case shapeType do
            "image" -> name <> ".png"
            "video" -> name
            _ -> ""
          end
        blipId =
          case shapeType do
            "image" ->
              every_pic["#{@p}blipFill"]["#{@a}blip"]["-#{@r}embed"]
            "video" ->
              every_pic["#{@p}nvPicPr"]["#{@p}nvPr"]["#{@a}videoFile"]["-#{@r}link"]
            _ ->
              ""
          end

        transMat =
          if shapeType == "video" do
            %{ Element.identityMat3() | m13: x, m23: y}
          else
            Element.identityMat3()
          end

        # Logger.warn "Media id :: #{blipId}"
        Logger.warn " #{shapeType}"
        %{ version: 1,
           type: shapeType,
           title: title,
           blipId: blipId,
           x: x,
           y: y,
           width: width,
           height: height,
           translateMatrix: transMat,
           rt: rt,
           rotateMatrix: Element.identityMat3(),
           parent_bbox: Element.initialBbox(),
           bbox: Element.initialBbox()
         }

    end

    defp dimensions(xfrm, rtString) do
      if is_nil(rtString) do
        Logger.warn "rot is nil"
        %{ x: xfrm["#{@a}off"]["-x"], #6096000
           y: xfrm["#{@a}off"]["-y"], #1055076
           width: xfrm["#{@a}ext"]["-cx"], #3600000 String.to_integer(x)/360000* 37.795275591
           height: xfrm["#{@a}ext"]["-cy"] #360000
        }
      else
        Logger.warn "rot is not nil"
        %{ x: xfrm["#content"]["#{@a}off"]["-x"], #6096000
           y: xfrm["#content"]["#{@a}off"]["-y"], #1055076
           width: xfrm["#content"]["#{@a}ext"]["-cx"], #3600000 String.to_integer(x)/360000* 37.795275591
           height: xfrm["#content"]["#{@a}ext"]["-cy"] #360000
        }
      end
    end

    def shapesElementsSP(every_sp) do
        # IO.inspect every_sp
        spPr = every_sp["#{@p}spPr"]
        xfrm = spPr["#{@a}xfrm"]
        rtString = xfrm["-rot"] #16200000
        dim = dimensions(xfrm, rtString)

        shapeType = toPrzType(every_sp, nil, nil)

        # Units conversions
        width = toPrzDimension(dim.width)
        height = toPrzDimension(dim.height)
        x = toPrzDimension(dim.x)
        y = toPrzDimension(dim.y)
        rt = toPrzRt(rtString)
        newM11 = rt
        |> Math.deg2rad
        |> Math.cos
        # |> Float.floor(6)

        newM12 = rt
        |> Math.deg2rad
        |> Math.sin
        # |> Float.floor(6)

        nextRotateMatrix =
          Element.identityMat3
          |> Map.put(:m11, newM11 )
          |> Map.put(:m12, - newM12 )
          |> Map.put(:m21, newM12 )
          |> Map.put(:m22, newM11 )

        case shapeType do
          "text" ->
              textBodyAp = every_sp["#{@p}txBody"]["#{@a}p"]
              # IO.inspect every_sp["#{@p}txBody"]
              textSettings =
                getTextSettings(textBodyAp)

              Logger.warn "#{inspect textSettings}"

              %{ version: 1,
                 type: shapeType,
                 content: "<p>" <> textSettings.content <> "</p>",
                 y: x,
                 x: y,
                 width: width,
                 height: height,
                 translateMatrix: Element.identityMat3(),
                 rt: rt,
                 rotateMatrix: nextRotateMatrix,
                 parent_bbox: Element.initialBbox(),
                 bbox: Element.initialBbox(),
                 lineHeight: textSettings.line_height
               }

          "circle" ->
            strokeWidth = toPrzStrokeWidth(spPr["#{@a}ln"]["-w"])
            strokeDasharray = toPrzDashArray(spPr["#{@a}ln"]["#content"]["#{@a}prstDash"])
            stroke = toPrzStroke(spPr["#{@a}ln"]["#content"]["#{@a}solidFill"]["#{@a}srgbClr"]["-val"])
            solidFillString = spPr["#{@a}solidFill"]

              %{ version: 1,
                 type: shapeType,
                 strokeWidth: strokeWidth,
                 stroke: stroke,
                 strokeDasharray: strokeDasharray,
                 rx: width / 2,
                 ry: height / 2,
                 fillOpacity: toPrzColorShade(solidFillString).shade,
                 fill: toPrzColorShade(solidFillString).color,
                 cx: x + (width / 2),
                 cy: y + (height / 2),
                 translateMatrix: Element.identityMat3(),
                 rt: rt,
                 rotateMatrix: nextRotateMatrix,
                 parent_bbox: Element.initialBbox(),
                 bbox: Element.initialBbox()
               }

          _ -> # Default case
              strokeWidth = toPrzStrokeWidth(spPr["#{@a}ln"]["-w"])
              strokeDasharray = toPrzDashArray(spPr["#{@a}ln"]["#content"]["#{@a}prstDash"])
              stroke = toPrzStroke(spPr["#{@a}ln"]["#content"]["#{@a}solidFill"]["#{@a}srgbClr"]["-val"])
              #Il faut déclarer une variable Opacity qui aura la valeur de alpha
              solidFillString = spPr["#{@a}solidFill"]

                %{ version: 1,
                   type: shapeType,
                   strokeWidth: strokeWidth,
                   strokeDasharray: strokeDasharray,
                   stroke: stroke,
                   ry: 0,
                   rx: 0,
                   fillOpacity: toPrzColorShade(solidFillString).shade,
                   fill: toPrzColorShade(solidFillString).color,
                   x: x,
                   y: y,
                   width: width,
                   height: height,
                   translateMatrix: Element.identityMat3(),
                   rt: rt,
                   rotateMatrix: nextRotateMatrix,
                   parent_bbox: Element.initialBbox(),
                   bbox: Element.initialBbox()
                 }

        end
    end

    # def elementsGraph(a, p, c, every_graph) do
    #     #récupérer de slide
    #     name = every_graph["#{@p}nvGraphicFramePr"]["#{@p}cNvPr"]["-name"]
    #     widthS = every_graph["#{@p}xfrm"]["#{@a}ext"]["-cx"]
    #     heightS = every_graph["#{@p}xfrm"]["#{@a}ext"]["-cy"]
    #     xS= every_graph["#{@p}xfrm"]["#{@a}off"]["-x"]
    #     yS= every_graph["#{@p}xfrm"]["#{@a}off"]["-y"]
    #
    #     # Units conversions
    #     width = toPrzDimension(widthS)
    #     height = toPrzDimension(heightS)
    #     x = toPrzDimension(xS)
    #     y = toPrzDimension(yS)
    #
    #     #récupérer de chart
    #
    #     shapeType = toPrzType(a, p, nil, nil, every_graph) #A modifier
    #
    #     textBodyAp =
    #       productMap["#{c}chartSpace"]["#{c}chart"]["#{c}title"]["#{c}tx"]["#{c}rich"]["#{@a}p"]
    #     gr =
    #       productMap["#{c}chartSpace"]["#{c}chart"]["#{c}title"]["#{c}tx"]["#{c}rich"]["#{@a}p"]
    #     title =
    #       gr["#{@a}r"]["#{@a}t"]
    #     checktext(a, "graph", title)
    #
    #       solidFillString = gr["#{@a}pPr"]["#{@a}defRPr"]["#content"]["#{@a}solidFill"]
    #
    #     %{ version: 1,
    #        type: shapeType,
    #        x: x,
    #        y: y,
    #        width: width,
    #        height: height,
    #        translateMatrix: Element.identityMat3(),
    #        rotateMatrix: Element.identityMat3(),
    #        parent_bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 },
    #        bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 },
    #        element_level: 1
    #      }
    #
    # end

    defp getTextSettings(textBodyAp) do
      # IO.inspect textBodyAp
      cond do
        # If the text is a paragraph
        is_list(textBodyAp) ->
            list_lines =
              Enum.map(textBodyAp, fn line ->
                textR = line["#{@a}r"]
                Logger.warn "#{inspect line}"
                if not is_nil(textR) do
                  IO.inspect textR
                  innerHtml =
                    cond do
                      is_list(textR) -> # if it is list of styled words
                        listStyledWords =
                          Enum.map(textR, fn word ->
                            create_word_style(word)
                          end)
                        "<div>" <> Enum.join(listStyledWords, "") <> "</div>"

                      true -> # If it's one word
                        # Logger.warn "#{inspect textR}"
                        "<div>" <> create_word_style(textR) <> "</div>"
                    end
                  else
                    IO.inspect line #line["#{@a}endParaRPr"]
                    ""
                end

                # %{ innerHtml: innerHtml, line_height: line_height}
             end)
            lineHeights =
              Enum.map(textBodyAp, fn line ->
                 Logger.debug "line #{inspect line}"
                 paragraphRpr = line["#{@a}pPr"]
                 # justification = toPrzTextPosition(paragraphRpr["-algn"])
                 # indent = toPrzTextIndent(paragraphRpr["-indent"])
                 # marL = toPrzParagraphMarge(paragraphRpr["-marL"])
                  #
                  #    # Logger.debug "Justification#{inspect justification}"
                  #    # Logger.debug "MARGE#{inspect marL}"
                  #    # Logger.debug "INDENT#{inspect indent}"
                  #    #dans le cas de puces
                  #    puceS = paragraphRpr["#content"]["#{@a}buChar"]["-char"]
                  #    puce = if is_nil(puceS) do "" else puceS end
                  #    numS = paragraphRpr["#content"]["#{@a}buAutoNum"]["-type"]
                  #    num = if is_nil(numS) do "" else numS end
                  #    Logger.debug "Puce#{inspect puce}"
                  #    Logger.debug "NUM#{inspect num}"
                 space_line = paragraphRpr["#{@a}lnSpc"]["#{@a}spcPct"]["-val"]
                 line_height = toPrzLineHeight(space_line)
                 # Logger.warn "#{inspect line_height}"

              end)
           %{ content: "<div>" <> Enum.join(list_lines, "") <> "</div>",
              line_height: List.first(lineHeights)
            }
           # [["<span>Phrase</span>", "<span>un</span>"], ["<span>Phrase</span>", "<span>deux</span>"]]

        # The text is one line (textBodyAp is one tag)
        true ->
          listOfWords = textBodyAp["#{@a}r"]
          IO.inspect listOfWords
          cond do
            is_list(listOfWords) ->
              styledLine = Enum.map(listOfWords, fn word ->
                create_word_style(word)
              end)
              %{ content: "<div>" <> Enum.join(styledLine, "") <> "</div>",
                 line_height: 1
               }
            true ->
              oneword = create_word_style(listOfWords)
              IO.inspect oneword
              %{ content: "<div>#{oneword}</div>",
                 line_height: 1
               }
          end

      end
    end

    defp toPrzType(every_sp, every_pic, every_graph) do
      formsName = every_sp["#{@p}nvSpPr"]["#{@p}cNvPr"]["-name"]
      picsName = every_pic["#{@p}nvPicPr"]["#{@p}cNvPr"]["-name"]
      graphName = every_graph["#{@p}nvGraphicFramePr"]["#{@p}cNvPr"]["-name"]
      # Logger.warn "Name #{inspect formsName}"
      # Logger.warn "Media name #{inspect picsName}"
      cond do
        not is_nil(formsName) ->
          [head | tail] =
            if not is_nil(formsName) do
              String.split(formsName)
            else
              ["unknown_type"]
            end
          IO.inspect head
          case head do
            "Ellipse" -> "circle"
            "Rectangle" -> "rectangle"
            "Triangle" -> "triangle"
            "Flèche :" -> "arrow"
            "Flèche" -> "arrow"
            "ZoneTexte" -> "text"
            "Titre" -> "text"
            "Sous-titre" -> "text"
            "Espace" -> "text"
            _ -> head
          end

        not is_nil(picsName) ->
          actionString = every_pic["#{@p}nvPicPr"]["#{@p}cNvPr"]["#content"]["#{@a}hlinkClick"]
          # actionString = every_pic["#{@p}nvPicPr"]["#{@p}nvPr"]["#{@a}videoFile"]
          # IO.inspect actionString
          cond do
            is_nil(actionString) ->
              "image"
            actionString["-action"] == "ppaction://media" ->
              "video"
            true ->
              "unknown_type"
          end

        not is_nil(graphName) ->
          [head | tail] =
            if not is_nil(graphName) do
              String.split(graphName)
            else
              ["unknown_type"]
            end
          # IO.inspect head
          case head do
            "Graphique" -> "przChart"
            _ -> head
          end

        true -> "unknown_type"
      end
    end
    defp toPrzOpacity(transparence) do
      if is_nil(transparence) do
       1
      else
        Float.floor(1 - ((String.to_integer transparence) / 100000), 2)
      end
    end

    defp toPrzDimension(widthString) do
      if is_nil(widthString) do
        0
      else
        # (String.to_integer(widthString) / 360000) * 37.795275591
        String.to_integer(widthString) / 10000
      end
    end

    defp toPrzRt(rtString) do

      if is_nil(rtString) do
         0
      else

        (String.to_integer(rtString) / 60000)
      end
    end

    defp toPrzStrokeWidth(val) do
      if is_nil(val) do
        0
      else
        String.to_integer(val) / 10000
      end
    end
    def toPrzDashArray(prstDash) do
      # Logger.warn "stroke dasharray #{inspect prstDash}"
      if is_nil(prstDash) do
        ""
      else
        cond do
          prstDash["-val"] == "dashDot" ->
            "2,2"
          prstDash["-val"] == "sysDot" ->
            "3,3"
          true ->
            ""
        end
      end
    end
    defp toPrzStroke(val) do
        if is_nil(val) do
          "#000000"
        else
          "##{val}"
        end
    end

    defp toPrzFontSize(spanSize) do
      if is_nil(spanSize) do
        "18px"
      else
        (to_string (String.to_integer(spanSize)/100)) <> "px"
      end
    end


    defp create_word_style(word) do
      # puceS = every_sp["#{@p}txBody"]["#{@a}p"]["#{@a}pPr"]["#content"]["#{@a}buChar"]["-char"]
      #                   puce = if is_nil(puceS) do "" else puceS end
      #                   numS = every_sp["#{@p}txBody"]["#{@a}p"]["#{@a}pPr"]["#content"]["#{@a}buAutoNum"]["-type"]
      #                   num = if is_nil(numS) do "" else numS end
      przstyle =
        if is_map(word) do
              rPr = word["#{@a}rPr"]
              color = toPrzWordColor(rPr).color

              font_weight =
                case rPr["-b"] do
                  "1" -> "font-weight: bold;"
                  _ -> ""
                end

              font_style =
                case rPr["-i"] do
                  "1" -> "font-style: italic;"
                  _ -> ""
                end

              sng = rPr["-u"]
              strike = rPr["-strike"]
              text_decoration =
                cond do
                  sng == "sng" ->
                    "text-decoration: underline;"
                  strike == "sngStrike" ->
                    "text-decoration: line-through;"
                  true ->
                    ""
                end
              textHighlight =
                rPr["#{@a}highlight"]["#{@a}srgbClr"]["-val"]

              font_size = toPrzFontSize(rPr["-sz"])
              spanPolice = rPr["#content"]["#{@a}latin"]["-typeface"]
              font_family =
                if is_nil(spanPolice) do
                  "font-family: Calibri;"
                else
                  "font-family: #{spanPolice};"
                end
              "color: #{color};#{font_family}#{font_weight}#{text_decoration}background-color: #{textHighlight};#{font_style}font-size: #{font_size};"
        else
            ""
        end

      # IO.inspect word
      innerText = word["#{@a}t"] <> " "
      # Logger.warn "#{inspect innerText}"
      # IO.inspect "innertext" <> innerText
      if innerText == " " || is_nil(innerText) || innerText == %{} do
        "&nbsp;"
      else
        "<span style='#{przstyle};'>" <> innerText <>"</span>"
      end
    end

    defp toPrzColorShade(solidFillString) do
      hexaColor = solidFillString["#{@a}srgbClr"]
      paletteColor = solidFillString["#{@a}schemeClr"]
      hexaShade = hexaColor["#content"]["#{@a}alpha"]
      # IO.inspect hexaColor
      # Logger.debug "hexaColor #{inspect hexaColor}"
      # Logger.debug "SchemeColor #{inspect paletteColor}"
      if not is_nil(paletteColor) do # Couleurs depuis les colonnes
      # Logger.warn "Color from Palette"
        # Transparence de palette
        lumMod = paletteColor["#content"]["#{@a}lumMod"]
        # Transparence des objets
        alpha = paletteColor["#content"]["#{@a}alpha"]
        # IO.inspect paletteColor["#content"]["#{@a}alpha"]
        [clr, sha ] = # Sub-case on shade
          if is_nil(lumMod) do
            [ paletteColor["-val"], "100000" ]
          else
            [paletteColor["-val"], lumMod["-val"] ] #return shade of color
          end
        %{color:  colorConversion("rectangle", clr, sha),
          shade: toPrzOpacity(alpha["-val"])
          } #remplacer 1 par toPrzOpacity(hexaShade["-val"] qui est la valeur de l'opacity

      else # Autres couleurs
        if is_nil(hexaShade) do
          lacouleur =
            if is_nil(hexaColor) do
              @defaultPPTShapeColor
            else
              "##{hexaColor["-val"]}"
            end
          %{color: lacouleur,
            shade: 1
          }
        else
          %{color: "##{hexaColor["-val"]}",
            shade: toPrzOpacity(hexaShade["-val"])
            } #hexashade ne doit pas exister car le shade est tjrs a 1 quand c en hexa
        end
      end
    end

    def colorConversion(shapeType, color, shadexml) do
      shade = to_string (String.to_integer(shadexml) / 1000)
      case color do
        "bg1" ->
          case shade do
            "65" -> "#a5a5a5"
            "75" -> "#bfbfbf"
            "50" -> "#7f7f7f"
            "85" -> "#d8d8d8"
            "95" -> "#f2f2f2"
            _ -> "#ffffff" #cas par défaut
          end

        "tx1" ->
          case shade do
            "85" -> "#262626"
            "75" -> "#3f3f3f"
            "95" -> "#0c0c0c"
            "65" -> "#595959"
            "50" -> "#7f7f7f"
            _ -> "#000000" #cas par défaut
          end

        "bg2" ->
          case shade do
            "25" -> "#3a3838"
            "50" -> "#757070"
            "10" -> "#171616"
            "75" -> "#aeabab"
            "90" -> "#d0cece"
            _ -> "#e7e6e6" #cas par défaut
          end

        "tx2" ->
          case shade do
            "75" -> "#323f4f"
            "60" -> "#8496b0"
            "50" -> "#222a35"
            "40" -> "#adb9ca"
            "20" -> "#d6dce4"
            _ -> "#44546a" #cas par défauts
          end
        "accent1" ->
          case shade do
            "75" -> "#2f5496"
            "60" -> "#8eaadb"
            "50" -> "#1f3864"
            "40" -> "#b4c6e7"
            "20" -> "#d9e2f3"
            _ -> "#4472c4" #cas par défaut
          end

        "accent2" ->
          case shade do
            "75" -> "#c55a11"
            "60" -> "#f4b183"
            "50" -> "#833c0b"
            "40" -> "#f7cbac"
            "20" -> "#fbe5d5"
               _ ->  "#ed7d31"  #cas par défaut
         end

       "accent3" ->
         case shade do
           "75" -> "#7b7b7b"
           "60" -> "#c9c9c9"
           "50" -> "#525252"
           "40" -> "#dbdbdb"
           "20" -> "#ededed"
           _ -> "#a5a5a5" #cas par défaut
         end

       "accent4" ->
         case shade do
           "75" -> "#bf9000"
           "60" -> "#ffd965"
           "50" -> "#7f6000"
           "40" -> "#fee599"
           "20" -> "#fff2cc"
           _ -> "#ffc000" #cas par défaut
         end

       "accent5" ->
         case shade do
           "75" -> "#2e75b5"
           "60" -> "#9cc3e5"
           "50" -> "#1e4e79"
           "40" -> "#bdd7ee"
           "20" -> "#deebf6"
           _ -> "#5b9bd5" #cas par défaut
         end

       "accent6" ->
         case shade do
           "75" -> "#538135"
           "60" -> "#a8d08d"
           "50" -> "#375623"
           "40" -> "#c5e0b3"
           "20" -> "#e2efd9"
           _ -> "#70ad47" #cas par défaut
         end
       _->
          if shapeType == "text" do
            "#000000"
          else
            @defaultPPTShapeColor
          end
      end
  end

  def toPrzTextPosition(justificationS) do
    if is_nil(justificationS) do
      "left"
    else
      justificationS
    end
  end

  def toPrzTextIndent(indentS) do
    if is_nil(indentS) do
      "0"
    else
      indentS
    end
  end

  def toPrzParagraphMarge(marLS) do
    if is_nil(marLS) do "0" else marLS end
  end

  def toPrzWordColor(wordrPr) do
      if is_nil(wordrPr["#content"]) do
        %{ color: "#000000",
           opacity: 1
         }
      else
        solidfill = wordrPr["#content"]["#{@a}solidFill"]
        spanHexaColor = solidfill["#{@a}srgbClr"]
        # Logger.warn "#{inspect spanHexaColor}"
        if not is_nil(spanHexaColor) do # Autres couleurs
          # Logger.warn "Autres couleurs"
          %{ color: "##{spanHexaColor["-val"]}",
             opacity: 1,

           }
        else # palettes
          # Logger.warn "Palettes couleurs"
          spanStrColor = solidfill["#{@a}schemeClr"]
          spanShadeString = spanStrColor["#content"]["#{@a}lumMod"]
          palresult =
            if is_nil(spanShadeString) do
              %{ color: spanStrColor["-val"],
                 opacity: "100000"
               }
            else
              %{ color: spanStrColor["-val"],
                 opacity: spanShadeString["-val"]
               }
            end
          %{ color: colorConversion("text", palresult.color, palresult.opacity ),
              opacity: 1 }
        end

      end
  end

  def toPrzLineHeight(space_line) do
    if is_nil(space_line) do
      1
    else
      String.to_integer(space_line)/100000
    end
  end

end
