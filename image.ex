defmodule PrzXmlToMapWeb.Import do

  require Logger
  import SweetXml
  import XmlToMap

  def run(_args) do
    Mix.Task.run("app.start")
    get_graph()
  end


  def colorConversion(color, shadexml) do

      shade = to_string (String.to_integer(shadexml) / 1000)
      case color do
        "bg1" ->
          case shade do
            "65" -> "#a5a5a5"
            "75" -> "#bfbfbf"
            "50" -> "#7f7f7f"
            "85" -> "#d8d8d8"
            "95" -> "#f2f2f2"
            _ -> "#ffffff" #cas par défaut
          end

        "tx1" ->
          case shade do
            "85" -> "#262626"
            "75" -> "#3f3f3f"
            "95" -> "#0c0c0c"
            "65" -> "#595959"
            "50" -> "#7f7f7f"
            _ -> "#000000" #cas par défaut
          end

        "bg2" ->
          case shade do
            "25" -> "#3a3838"
            "50" -> "#757070"
            "10" -> "#171616"
            "75" -> "#aeabab"
            "90" -> "#d0cece"
            _ -> "#e7e6e6" #cas par défaut
          end

        "tx2" ->
          case shade do
            "75" -> "#323f4f"
            "60" -> "#8496b0"
            "50" -> "#222a35"
            "40" -> "#adb9ca"
            "20" -> "#d6dce4"
            _ -> "#44546a" #cas par défauts
          end
          "accent1" ->
            case shade do
              "75" -> "#2f5496"
              "60" -> "#8eaadb"
              "50" -> "#1f3864"
              "40" -> "#b4c6e7"
              "20" -> "#d9e2f3"
              _ -> "#4472c4" #cas par défaut
            end

          "accent2" ->
            case shade do
              "75" -> "#c55a11"
              "60" -> "#f4b183"
              "50" -> "#833c0b"
              "40" -> "#f7cbac"
              "20" -> "#fbe5d5"
                 _ ->  "#ed7d31"  #cas par défaut
           end

         "accent3" ->
           case shade do
             "75" -> "#7b7b7b"
             "60" -> "#c9c9c9"
             "50" -> "#525252"
             "40" -> "#dbdbdb"
             "20" -> "#ededed"
             _ -> "#a5a5a5" #cas par défaut
           end

         "accent4" ->
           case shade do
             "75" -> "#bf9000"
             "60" -> "#ffd965"
             "50" -> "#7f6000"
             "40" -> "#fee599"
             "20" -> "#fff2cc"
             _ -> "#ffc000" #cas par défaut
           end

         "accent5" ->
           case shade do
             "75" -> "#2e75b5"
             "60" -> "#9cc3e5"
             "50" -> "#1e4e79"
             "40" -> "#bdd7ee"
             "20" -> "#deebf6"
             _ -> "#5b9bd5" #cas par défaut
           end

         "accent6" ->
           case shade do
             "75" -> "#538135"
             "60" -> "#a8d08d"
             "50" -> "#375623"
             "40" -> "#c5e0b3"
             "20" -> "#e2efd9"
             _ -> "#70ad47" #cas par défaut
           end
         #_->
      end
  end


  def get_graph() do


    data = ~S(<?xml version="1.0" encoding="UTF-8" standalone="yes"?><c:chartSpace xmlns:c="http://schemas.openxmlformats.org/drawingml/2006/chart" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:c16r2="http://schemas.microsoft.com/office/drawing/2015/06/chart"><c:date1904 val="0"/><c:lang val="fr-FR"/><c:roundedCorners val="0"/><mc:AlternateContent xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"><mc:Choice Requires="c14" xmlns:c14="http://schemas.microsoft.com/office/drawing/2007/8/2/chart"><c14:style val="102"/></mc:Choice><mc:Fallback><c:style val="2"/></mc:Fallback></mc:AlternateContent><c:chart><c:title><c:tx><c:rich><a:bodyPr rot="0" spcFirstLastPara="1" vertOverflow="ellipsis" vert="horz" wrap="square" anchor="ctr" anchorCtr="1"/><a:lstStyle/><a:p><a:pPr><a:defRPr sz="1862" b="0" i="0" u="none" strike="noStrike" kern="1200" spc="0" baseline="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="65000"/><a:lumOff val="35000"/></a:schemeClr></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:pPr><a:r><a:rPr lang="fr-FR"/><a:t>Titre ex</a:t></a:r><a:endParaRPr lang="fr-FR" dirty="0"/></a:p></c:rich></c:tx><c:overlay val="0"/><c:spPr><a:noFill/><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:txPr><a:bodyPr rot="0" spcFirstLastPara="1" vertOverflow="ellipsis" vert="horz" wrap="square" anchor="ctr" anchorCtr="1"/><a:lstStyle/><a:p><a:pPr><a:defRPr sz="1862" b="0" i="0" u="none" strike="noStrike" kern="1200" spc="0" baseline="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="65000"/><a:lumOff val="35000"/></a:schemeClr></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:pPr><a:endParaRPr lang="fr-FR"/></a:p></c:txPr></c:title><c:autoTitleDeleted val="0"/><c:plotArea><c:layout/><c:barChart><c:barDir val="col"/><c:grouping val="clustered"/><c:varyColors val="0"/><c:ser><c:idx val="0"/><c:order val="0"/><c:tx><c:strRef><c:f>Feuil1!$B$1</c:f><c:strCache><c:ptCount val="1"/><c:pt idx="0"><c:v>Série 1</c:v></c:pt></c:strCache></c:strRef></c:tx><c:spPr><a:solidFill><a:schemeClr val="accent1"/></a:solidFill><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:invertIfNegative val="0"/><c:cat><c:strRef><c:f>Feuil1!$A$2:$A$5</c:f><c:strCache><c:ptCount val="4"/><c:pt idx="0"><c:v>Catégorie 1</c:v></c:pt><c:pt idx="1"><c:v>Catégorie 2</c:v></c:pt><c:pt idx="2"><c:v>Catégorie 3</c:v></c:pt><c:pt idx="3"><c:v>Catégorie 4</c:v></c:pt></c:strCache></c:strRef></c:cat><c:val><c:numRef><c:f>Feuil1!$B$2:$B$5</c:f><c:numCache><c:formatCode>General</c:formatCode><c:ptCount val="4"/><c:pt idx="0"><c:v>4.3</c:v></c:pt><c:pt idx="1"><c:v>2.5</c:v></c:pt><c:pt idx="2"><c:v>3.5</c:v></c:pt><c:pt idx="3"><c:v>4.5</c:v></c:pt></c:numCache></c:numRef></c:val><c:extLst><c:ext uri="{C3380CC4-5D6E-409C-BE32-E72D297353CC}" xmlns:c16="http://schemas.microsoft.com/office/drawing/2014/chart"><c16:uniqueId val="{00000000-F0F6-43CC-901B-3D6BB0D071F4}"/></c:ext></c:extLst></c:ser><c:ser><c:idx val="1"/><c:order val="1"/><c:tx><c:strRef><c:f>Feuil1!$C$1</c:f><c:strCache><c:ptCount val="1"/><c:pt idx="0"><c:v>Série 2</c:v></c:pt></c:strCache></c:strRef></c:tx><c:spPr><a:solidFill><a:schemeClr val="accent2"/></a:solidFill><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:invertIfNegative val="0"/><c:cat><c:strRef><c:f>Feuil1!$A$2:$A$5</c:f><c:strCache><c:ptCount val="4"/><c:pt idx="0"><c:v>Catégorie 1</c:v></c:pt><c:pt idx="1"><c:v>Catégorie 2</c:v></c:pt><c:pt idx="2"><c:v>Catégorie 3</c:v></c:pt><c:pt idx="3"><c:v>Catégorie 4</c:v></c:pt></c:strCache></c:strRef></c:cat><c:val><c:numRef><c:f>Feuil1!$C$2:$C$5</c:f><c:numCache><c:formatCode>General</c:formatCode><c:ptCount val="4"/><c:pt idx="0"><c:v>2.4</c:v></c:pt><c:pt idx="1"><c:v>4.4000000000000004</c:v></c:pt><c:pt idx="2"><c:v>1.8</c:v></c:pt><c:pt idx="3"><c:v>2.8</c:v></c:pt></c:numCache></c:numRef></c:val><c:extLst><c:ext uri="{C3380CC4-5D6E-409C-BE32-E72D297353CC}" xmlns:c16="http://schemas.microsoft.com/office/drawing/2014/chart"><c16:uniqueId val="{00000001-F0F6-43CC-901B-3D6BB0D071F4}"/></c:ext></c:extLst></c:ser><c:ser><c:idx val="2"/><c:order val="2"/><c:tx><c:strRef><c:f>Feuil1!$D$1</c:f><c:strCache><c:ptCount val="1"/><c:pt idx="0"><c:v>Série 3</c:v></c:pt></c:strCache></c:strRef></c:tx><c:spPr><a:solidFill><a:schemeClr val="accent3"/></a:solidFill><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:invertIfNegative val="0"/><c:cat><c:strRef><c:f>Feuil1!$A$2:$A$5</c:f><c:strCache><c:ptCount val="4"/><c:pt idx="0"><c:v>Catégorie 1</c:v></c:pt><c:pt idx="1"><c:v>Catégorie 2</c:v></c:pt><c:pt idx="2"><c:v>Catégorie 3</c:v></c:pt><c:pt idx="3"><c:v>Catégorie 4</c:v></c:pt></c:strCache></c:strRef></c:cat><c:val><c:numRef><c:f>Feuil1!$D$2:$D$5</c:f><c:numCache><c:formatCode>General</c:formatCode><c:ptCount val="4"/><c:pt idx="0"><c:v>2</c:v></c:pt><c:pt idx="1"><c:v>2</c:v></c:pt><c:pt idx="2"><c:v>3</c:v></c:pt><c:pt idx="3"><c:v>5</c:v></c:pt></c:numCache></c:numRef></c:val><c:extLst><c:ext uri="{C3380CC4-5D6E-409C-BE32-E72D297353CC}" xmlns:c16="http://schemas.microsoft.com/office/drawing/2014/chart"><c16:uniqueId val="{00000002-F0F6-43CC-901B-3D6BB0D071F4}"/></c:ext></c:extLst></c:ser><c:dLbls><c:showLegendKey val="0"/><c:showVal val="0"/><c:showCatName val="0"/><c:showSerName val="0"/><c:showPercent val="0"/><c:showBubbleSize val="0"/></c:dLbls><c:gapWidth val="219"/><c:overlap val="-27"/><c:axId val="147998240"/><c:axId val="201584848"/></c:barChart><c:catAx><c:axId val="147998240"/><c:scaling><c:orientation val="minMax"/></c:scaling><c:delete val="0"/><c:axPos val="b"/><c:numFmt formatCode="General" sourceLinked="1"/><c:majorTickMark val="none"/><c:minorTickMark val="none"/><c:tickLblPos val="nextTo"/><c:spPr><a:noFill/><a:ln w="9525" cap="flat" cmpd="sng" algn="ctr"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="15000"/><a:lumOff val="85000"/></a:schemeClr></a:solidFill><a:round/></a:ln><a:effectLst/></c:spPr><c:txPr><a:bodyPr rot="-60000000" spcFirstLastPara="1" vertOverflow="ellipsis" vert="horz" wrap="square" anchor="ctr" anchorCtr="1"/><a:lstStyle/><a:p><a:pPr><a:defRPr sz="1197" b="0" i="0" u="none" strike="noStrike" kern="1200" baseline="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="65000"/><a:lumOff val="35000"/></a:schemeClr></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:pPr><a:endParaRPr lang="fr-FR"/></a:p></c:txPr><c:crossAx val="201584848"/><c:crosses val="autoZero"/><c:auto val="1"/><c:lblAlgn val="ctr"/><c:lblOffset val="100"/><c:noMultiLvlLbl val="0"/></c:catAx><c:valAx><c:axId val="201584848"/><c:scaling><c:orientation val="minMax"/></c:scaling><c:delete val="0"/><c:axPos val="l"/><c:majorGridlines><c:spPr><a:ln w="9525" cap="flat" cmpd="sng" algn="ctr"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="15000"/><a:lumOff val="85000"/></a:schemeClr></a:solidFill><a:round/></a:ln><a:effectLst/></c:spPr></c:majorGridlines><c:numFmt formatCode="General" sourceLinked="1"/><c:majorTickMark val="none"/><c:minorTickMark val="none"/><c:tickLblPos val="nextTo"/><c:spPr><a:noFill/><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:txPr><a:bodyPr rot="-60000000" spcFirstLastPara="1" vertOverflow="ellipsis" vert="horz" wrap="square" anchor="ctr" anchorCtr="1"/><a:lstStyle/><a:p><a:pPr><a:defRPr sz="1197" b="0" i="0" u="none" strike="noStrike" kern="1200" baseline="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="65000"/><a:lumOff val="35000"/></a:schemeClr></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:pPr><a:endParaRPr lang="fr-FR"/></a:p></c:txPr><c:crossAx val="147998240"/><c:crosses val="autoZero"/><c:crossBetween val="between"/></c:valAx><c:spPr><a:noFill/><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr></c:plotArea><c:legend><c:legendPos val="b"/><c:overlay val="0"/><c:spPr><a:noFill/><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:txPr><a:bodyPr rot="0" spcFirstLastPara="1" vertOverflow="ellipsis" vert="horz" wrap="square" anchor="ctr" anchorCtr="1"/><a:lstStyle/><a:p><a:pPr><a:defRPr sz="1197" b="0" i="0" u="none" strike="noStrike" kern="1200" baseline="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="65000"/><a:lumOff val="35000"/></a:schemeClr></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:pPr><a:endParaRPr lang="fr-FR"/></a:p></c:txPr></c:legend><c:plotVisOnly val="1"/><c:dispBlanksAs val="gap"/><c:extLst><c:ext uri="{56B9EC1D-385E-4148-901F-78D8002777C0}" xmlns:c16r3="http://schemas.microsoft.com/office/drawing/2017/03/chart"><c16r3:dataDisplayOptions16><c16r3:dispNaAsBlank val="1"/></c16r3:dataDisplayOptions16></c:ext></c:extLst><c:showDLblsOverMax val="0"/></c:chart><c:spPr><a:noFill/><a:ln><a:noFill/></a:ln><a:effectLst/></c:spPr><c:txPr><a:bodyPr/><a:lstStyle/><a:p><a:pPr><a:defRPr/></a:pPr><a:endParaRPr lang="fr-FR"/></a:p></c:txPr><c:externalData r:id="rId3"><c:autoUpdate val="0"/></c:externalData></c:chartSpace>)

       productMap =  XmlToMap.naive_map(data)

       p = "{http://schemas.openxmlformats.org/presentationml/2006/main}"
       c = "{http://schemas.openxmlformats.org/drawingml/2006/chart}"
       a = "{http://schemas.openxmlformats.org/drawingml/2006/main}"


       #Ceci est à réupérer dans la dossier slide
       # name = productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}graphicFrame"]["#{p}nvGraphicFramePr"]["#{p}cNvPr"]["-name"]
       # Logger.debug "name#{inspect name}"
       # widthS = productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}graphicFrame"]["#{p}xfrm"]["#{a}ext"]["-cx"]
       # Logger.debug "widthS#{inspect widthS}"
       # heightS = productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}graphicFrame"]["#{p}xfrm"]["#{a}ext"]["-cy"]
       # Logger.debug "heightS#{inspect heightS}"
       # xS= productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}graphicFrame"]["#{p}xfrm"]["#{a}off"]["-x"]
       # Logger.debug "xS#{inspect xS}"
       # yS= productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}graphicFrame"]["#{p}xfrm"]["#{a}off"]["-y"]
       # Logger.debug "yS#{inspect yS}"

       #      width = toPrzDimension(widthS)
       #      height = toPrzDimension(heightS)
      #       x = toPrzDimension(xS)
      #       y = toPrzDimension(yS)


       # #ceci est à récupérer dans le dossier charts

       gr = productMap["#{c}chartSpace"]["#{c}chart"]["#{c}title"]["#{c}tx"]["#{c}rich"]["#{a}p"]

       title = gr["#{a}r"]["#{a}t"] #on fait appelle à la fonction de vérification text
       Logger.debug "title#{inspect title}"
       barre = gr["#{a}pPr"]["#{a}defRPr"]["-strike"]
       bold = gr["#{a}pPr"]["#{a}defRPr"]["-b"]
       spc = gr["#{a}pPr"]["#{a}defRPr"]["-spc"]
       italic = gr["#{a}pPr"]["#{a}defRPr"]["-i"]
       souligne = gr["#{a}pPr"]["#{a}defRPr"]["-u"]
       size = gr["#{a}pPr"]["#{a}defRPr"]["-sz"]# on appelle la fonction prz
       couleurPalette = gr["#{a}pPr"]["#{a}defRPr"]["#content"]["#{a}solidFill"]["#{a}schemeClr"]["-val"]
       couleur = gr["#{a}pPr"]["#{a}defRPr"]["#content"]["#{a}solidFill"]["#{a}srgbClr"]["#content"]["-val"]
       shade = gr["#{a}pPr"]["#{a}defRPr"]["#content"]["#{a}solidFill"]["#{a}schemeClr"]["#content"]["#{a}lumMod"]["-val"]

       liste = productMap["#{c}chartSpace"]["#{c}chart"]["#{c}plotArea"]["#{c}barChart"]["#{c}ser"]

      s = List.first(liste)
      serie = s["#{c}cat"]["#{c}strRef"]["#{c}strCache"]["#{c}pt"]
      lescategories =
        Enum.map(serie, fn seri ->
          seriiie = seri["#content"]["#{c}v"]
        #Logger.debug "serie#{inspect seriiie}"
        end)


      therest =
        Enum.map(liste, fn ser ->
           series = ser["#{c}tx"]["#{c}strRef"]["#{c}strCache"]["#{c}pt"]["#content"]["#{c}v"]
           #Logger.debug "serie#{inspect series}"
           values = ser["#{c}val"]["#{c}numRef"]["#{c}numCache"]["#{c}pt"]
            lesvaleurs =
              Enum.map(values, fn val ->
                 valeur = val["#content"]["#{c}v"]
                #Logger.debug "valeur#{inspect valeur}"
           end)
           [ series] ++ lesvaleurs
         end)
      tab2D = [[""] ++ lescategories] ++ therest
      transpose(tab2D)
  end

def transpose([]), do: []
def transpose([[]|_]), do: []
def transpose(a) do
  [Enum.map(a, &hd/1) | transpose(Enum.map(a, &tl/1))]
end



end
