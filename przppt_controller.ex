defmodule PrezanceWeb.Dataset.PrzpptController do
  use PrezanceWeb, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: PrezanceWeb.HttpErrorHandler

  alias Prezance.Documents.{Document, Element}
  alias PrezanceWeb.Dataset.PptImport

  @pptPath (Application.get_env(:prezance, :file_uploads_dir) <> "pptfiles")
  @elementsDir Application.get_env(:prezance, :file_uploads_dir) <> "images/elements"
  @videosDir Application.get_env(:prezance, :file_uploads_dir) <> "videos"
  @p "{http://schemas.openxmlformats.org/presentationml/2006/main}"
  @a "{http://schemas.openxmlformats.org/drawingml/2006/main}"
  @c "{http://schemas.openxmlformats.org/drawingml/2006/chart}"
  @r "{http://schemas.openxmlformats.org/officeDocument/2006/relationships}"

  def msUpload(conn, params) do
    currentUserId = Guardian.Plug.current_resource(conn).id
    fullfilename = params["filename"]
    content = params["content"]
    [ _datafile_mime, encoded ] = String.split(content, ";base64,")
    binary = Base.decode64!(encoded)
    # IO.inspect binary
    case params["file"] do
      "ppt" ->
        [filename, _file_type] = String.split(fullfilename, ".")
        Logger.info "Imported file name #{inspect filename}"
        compressedFilePath = @pptPath <> "/#{currentUserId}_#{filename}.zip"
        # Writing PPT to ZIP
        case File.open(compressedFilePath, [:write]) do
          {:ok, io_device}->
            IO.binwrite(io_device, binary)
            File.close(io_device)
            # Extracting ZIP content
            cwd = @pptPath <> "/#{currentUserId}_temp_xtract"
            case :zip.unzip(String.to_charlist(compressedFilePath), [{:cwd, String.to_charlist(cwd)}]) do
              {:ok, _extraction } -> # Reading every slide in ppt/slides
                rendered_document = readExtractedPPT(conn, filename, cwd)
                File.rm_rf!(@pptPath <> "/#{currentUserId}_#{filename}.zip")
                File.rm_rf!(@pptPath <> "/#{currentUserId}_temp_xtract")
                conn  |> put_view(PrezanceWeb.Documents.DocumentView)  |> render("new_doc.json", %{document: rendered_document })
              {:error, reason} ->
                Logger.error "#{inspect reason}"
            end

          {:error, reason} ->
            PrzUtils.generate_errors("open", reason)
            render(conn, "error.json", %{error: "Error in console while writing ppt .zip file"})
        end
      _ ->
        render(conn, "error.json", %{error: "No file type xl in url"})
    end
  end

  defp readExtractedPPT(conn, pptName, cwd) do
    currentUserId = Guardian.Plug.current_resource(conn).id
    pptSlidesPath = cwd <> "/ppt/slides/"
    case File.ls(pptSlidesPath) do
      {:ok, files } ->
        changeset = Document.changeset_create_fst_doc(%Document{}, %{
          variant_name: pptName,
          variant: 0,
          owner_id: currentUserId
          })
        case Repo.insert(changeset) do
          {:ok, doc} ->
            Enum.each(files, fn slidexml ->
              Logger.warn "Slide #{inspect slidexml}"
              readSlideXML(conn, doc, slidexml, pptSlidesPath)
            end)
            doc
              |> Repo.preload(:owner)
              |> Repo.preload(:slides)
              |> Repo.preload(slides: :owner)


          {:error, cs }->
            Logger.error "#{inspect cs.errors}"
        end

      {:error, reason} ->
        PrzUtils.generate_errors("ls", reason)
      end
  end

  defp readSlideXML(conn, doc, slidexml, pptSlidesPath) do
    currentUserId = Guardian.Plug.current_resource(conn).id
        #ex. slidexml = slide12.xml
        if String.starts_with?(slidexml, "slide") do
          [_a, b] = String.split(slidexml, "slide")
          [position, _d ] = String.split(b, ".")

          case File.open(pptSlidesPath <> slidexml, [:read, :utf8]) do
            {:ok, dev} ->
              read_data = IO.read(dev, :all)
              data = String.replace(read_data, "\n", "")
              # IO.inspect data
              productMap = XmlToMap.naive_map(data)

              bgImgBlip =
                productMap["#{@p}sld"]["#{@p}cSld"]["#{@p}bgPr"]["#{@a}blipFill"]["#{@a}blip"]
              slide =
                if is_nil(bgImgBlip) do
                  # create_slide has document_slide creation by default
                  PrzUtils.create_slide(%{
                    visibility: 1,
                    owner_id: doc.owner_id,
                    canvas: "{}",
                    type: "slide"
                    }, position, doc.id)
                # else
                #   case File.open(@pptPath <> "/#{currentUserId}_temp_xtract/ppt/slides/_rels/slide#{position}.xml.rels", [:read, :utf8]) do
                #     {:ok, slide_rel} ->
                #       tom = IO.read(slide_rel, :all)
                #       |> XmlToMap.naive_map()
                #       # IO.inspect tom
                #       findedImg =
                #         Enum.find(tom["Relationships"]["Relationship"], fn x ->
                #           (List.last(String.split(x["-Type"], "/")) == "image") && (x["-Id"] == bgImgBlip["-#{@r}embed"])
                #         end)
                #       Logger.warn "#{inspect findedImg}"
                #       if not is_nil(findedImg) do
                #         mediaName = List.last(String.split(findedImg["-Target"], "/"))
                #         moveMediaToPrz(mediaPath, mediaName, destination)
                #         # create_slide has document_slide creation by default
                #         PrzUtils.create_slide(%{
                #           visibility: 1,
                #           owner_id: doc.owner_id,
                #           canvas: "{}",
                #           type: "slide"
                #           background_type: "image",
                #           background_link: "/uploads/images/elements/#{id}#{mediaName}.png"
                #           }, position, doc.id)
                #       end
                #
                #       File.close(slide_rel)
                #     {:error, reason } ->
                #       PrzUtils.generate_errors("open", reason)
                #   end
                end
              # IO.inspect productMap
              # Logger.warn "#{inspect ppt_mime}"
              create_prz_map_from_xml(currentUserId, slide, productMap)

              File.close(dev)
            {:error, reason } ->
              PrzUtils.generate_errors("open", reason)
          end
      end
  end

  defp create_prz_map_from_xml(currentUserId, slide, productMap) do
    # IO.inspect productMap["#{@p}sld"]
    spTree =
      productMap["#{@p}sld"]["#{@p}cSld"]["#{@p}spTree"]

    # IO.inspect spTree
    listOfSp =
      spTree["#{@p}sp"]
    listOfpic =
      spTree["#{@p}pic"]
    listOfgraphicFrame =
      spTree["#{@p}graphicFrame"]

    # FlÃ¨cheÂ : droite 4
    # Logger.warn "#{inspect listOfSp}"
    listOfMedias =
      cond do
        is_list(listOfpic) ->
          Enum.map(listOfpic, fn every_pic ->
            PptImport.mediaElementsPic(every_pic)
          end)
        is_nil(listOfpic) ->
          []
        true ->
          [PptImport.mediaElementsPic(listOfpic)]
      end

    # listOfCharts =
    #   cond do
    #     is_list(listOfgraphicFrame) ->
    #       case File.open("/ppt/charts/") do
    #         {:ok, chart} ->
    #           Enum.map(listOfgraphicFrame, fn every_gf ->
    #             PptImport.elementsGraph(every_gf)
    #           end)
    #         {:error, reason} ->
    #           Logger.error "#{inspect reason}"
    #       end
    #
    #     is_nil(listOfgraphicFrame) -> []
    #     true ->
    #       # IO.inspect listOfSp
    #       [ PptImport.elementsGraph(listOfgraphicFrame) ]
    #   end

    listOfShapes =
      cond do
        is_list(listOfSp) ->
          Enum.map(listOfSp, fn every_sp ->
            PptImport.shapesElementsSP(every_sp)
          end)
        is_nil(listOfSp) ->
          []
        true ->
          # IO.inspect listOfSp
          element = PptImport.shapesElementsSP(listOfSp)
          [ element ]
      end

    allElements = listOfMedias ++ listOfShapes
    |> Enum.with_index
    |> Enum.map(fn {e, index} -> Map.put(e, :element_level, index) end)

    create_ppt_elements(currentUserId, slide, allElements)


  end

  defp create_ppt_elements(currentUserId, slide, allElements) do
    Enum.each(allElements, fn e ->
      element = PrzUtils.map_keys_to_strings(e)
      # Splitting the json to take position elsewhere
      %{elementJSON: elementJSON, slideElementJSON: slideElementJSON } =
        PrzUtils.filter_element_jsonObject(element, element["type"])

          changeset = Element.changeset_create_fst_version(%Element{}, %{
              jsonObject: elementJSON,
               owner_id: currentUserId,
               type: element["type"]
             })
          case Repo.insert(changeset) do
            {:ok, insertedElement} ->
              Logger.warn "element inserted youuuhooooo"
              ### Moving files to our directory
              if insertedElement.type == "image" || insertedElement.type == "video" do
                copy_images_to_prz_folder(currentUserId, slide, insertedElement, element["blipId"])
              end

              PrzUtils.create_slide_element(%{
                slide_id: slide.id,
                element_id: insertedElement.id,
                element_level: element["element_level"],
                element_position: slideElementJSON
              })




            {:error, cs} ->
              Logger.error "#{inspect cs.errors}"
          end
    end)
  end


  defp copy_images_to_prz_folder(currentUserId, slide, insertedElement, blipId) do
    mediaPath = @pptPath <> "/#{currentUserId}_temp_xtract/ppt/media"
    decodedImageJson = Poison.decode!(insertedElement.jsonObject)
    destination =
      case insertedElement.type do
        "image" ->
          @elementsDir <> "/#{insertedElement.element_id}#{decodedImageJson["title"]}"
        "video" ->
          @videosDir <> "/#{insertedElement.element_id}-vid.mp4"
        _ -> ""
      end

    case File.open(@pptPath <> "/#{currentUserId}_temp_xtract/ppt/slides/_rels/slide#{slide.slide_position}.xml.rels", [:read, :utf8]) do
      {:ok, slide_rel} ->
        read_data = IO.read(slide_rel, :all)
        tom = XmlToMap.naive_map(read_data)
        # IO.inspect tom
        findedImg =
          Enum.find(tom["Relationships"]["Relationship"], fn x ->
            (List.last(String.split(x["-Type"], "/")) == insertedElement.type) && (x["-Id"] == blipId)
          end)
        Logger.warn "#{inspect findedImg}"
        if not is_nil(findedImg) do
          mediaName = List.last(String.split(findedImg["-Target"], "/"))
          moveMediaToPrz(mediaPath, mediaName, destination)
        end

        File.close(slide_rel)
      {:error, reason } ->
        PrzUtils.generate_errors("open", reason)
    end
  end

  defp moveMediaToPrz(mediaPath, mediaName, destination) do
    # If image it's duplicated we should copy the image one time
    # with an element_id which is common for the other elements
    case File.cp(mediaPath <> "/" <> mediaName, destination) do
      :ok -> Logger.info "Media moved successfully to uploads/images"
      {:error, reason} -> PrzUtils.generate_errors("rename", reason)
    end
  end
end
