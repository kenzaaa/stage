defmodule XmlToMapWeb.Import do

  import SweetXml

  def run(_args) do
    Mix.Task.run("app.start")
    import_dump()
  end

  def import_dump do


            doc = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<p:sld xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="4" name="Rectangle 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{13AD18C6-1721-4F04-9E7D-34B8DD89DB89}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm rot="16200000"><a:off x="1252024" y="1350498"/><a:ext cx="3600000" cy="1800000"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:solidFill><a:schemeClr val="accent4"><a:lumMod val="60000"/><a:lumOff val="40000"/><a:alpha val="60000"/></a:schemeClr></a:solidFill><a:ln w="76200"><a:solidFill><a:srgbClr val="FF0000"/></a:solidFill></a:ln></p:spPr><p:style><a:lnRef idx="2"><a:schemeClr val="accent1"><a:shade val="50000"/></a:schemeClr></a:lnRef><a:fillRef idx="1"><a:schemeClr val="accent1"/></a:fillRef><a:effectRef idx="0"><a:schemeClr val="accent1"/></a:effectRef><a:fontRef idx="minor"><a:schemeClr val="lt1"/></a:fontRef></p:style><p:txBody><a:bodyPr rtlCol="0" anchor="ctr"/><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:endParaRPr lang="fr-FR"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Ellipse 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{290CF723-700B-4ACF-9968-130226CD4378}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm rot="600000"><a:off x="6096000" y="1055076"/><a:ext cx="3600000" cy="360000"/></a:xfrm><a:prstGeom prst="ellipse"><a:avLst/></a:prstGeom><a:solidFill><a:srgbClr val="FFFF00"><a:alpha val="80000"/></a:srgbClr></a:solidFill><a:ln><a:solidFill><a:srgbClr val="002060"/></a:solidFill></a:ln></p:spPr><p:style><a:lnRef idx="2"><a:schemeClr val="accent1"><a:shade val="50000"/></a:schemeClr></a:lnRef><a:fillRef idx="1"><a:schemeClr val="accent1"/></a:fillRef><a:effectRef idx="0"><a:schemeClr val="accent1"/></a:effectRef><a:fontRef idx="minor"><a:schemeClr val="lt1"/></a:fontRef></p:style><p:txBody><a:bodyPr rtlCol="0" anchor="ctr"/><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:endParaRPr lang="fr-FR"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="6" name="ZoneTexte 5"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{B5918D08-898B-4911-87B5-3AB7A43EAB84}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="6316394" y="3671668"/><a:ext cx="3108960" cy="707886"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="square" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:r><a:rPr lang="fr-FR" sz="4000" b="1" i="1" u="sng" dirty="0"><a:solidFill><a:schemeClr val="bg1"/></a:solidFill><a:highlight><a:srgbClr val="00FF00"/></a:highlight></a:rPr><a:t>Essai</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="2" name="ZoneTexte 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{4B32F24B-83D3-415F-81C5-C41F2E4482B7}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="2588455" y="4937759"/><a:ext cx="4079631" cy="707886"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="square" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="fr-FR" sz="2800" b="1" i="1" dirty="0"/><a:t>Salut</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"><a:latin typeface="Abadi" panose="020B0604020104020204" pitchFamily="34" charset="0"/></a:rPr><a:t>Kenza</a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" sz="4000" dirty="0"/><a:t>ca</a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t>va</a:t></a:r></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="4071009222"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>
                  """

             #result = doc|> xpath(~x"//p:spTree/p:nvGrpSpPr/p:cNvPr/@id")


             result = doc |> xmap(
               Contenu: [

                    ~x"//p:cSld/p:spTree/p:sp"l,
                    type: ~x"./p:nvSpPr/p:cNvPr/@name[1]"s |> SweetXml.transform_by(fn x -> hd(String.split(x)) end),
                    y: ~x"./p:spPr/a:xfrm/a:off/@y[1]",#y
                    x: ~x"./p:spPr/a:xfrm/a:off/@x[1]",#x
                    height:  ~x"./p:spPr/a:xfrm/a:ext/@cy[1]"s |> SweetXml.transform_by(fn x -> String.to_integer(x)/360000* 37.795275591 end) ,#cy #hauteur
                    width:  ~x"./p:spPr/a:xfrm/a:ext/@cx[1]"s |> SweetXml.transform_by(fn x -> String.to_integer(x)/360000* 37.795275591 end), #cx #largeur
                    Couleur: ~x"./p:spPr/a:solidFill/a:schemeClr/@val[1]", #on récupére chaine de caractere
                    Couleur2: ~x"./p:spPr/a:solidFill/a:srgbClr/@val[1]", #on récupére un hexa
                    opacityShemaCLR: ~x"./p:spPr/a:solidFill/a:schemeClr/a:alpha/@val[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else ((100000 - String.to_integer(x)))/1000 end end),
                    opacitySrgbCLR: ~x"./p:spPr/a:solidFill/a:srgbClr/a:alpha/@val[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else ((100000 - String.to_integer(x)))/1000 end end),
                    ClrShade: ~x"./p:spPr/a:solidFill/a:schemeClr/a:lumOff/@val[1]",
                    CouleurBordure: ~x"./p:spPr/a:ln/a:solidFill/a:srgbClr/@val[1]", #couleur des bordures
                    Rotation: ~x"./p:spPr/a:xfrm/@rot[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else String.to_integer(x)/60000 end end), #En degré
                    #width: ~x"./p:spPr/a:ln/@w"l, #c pas la bonne taille
                    #pointille: ~x"./p:spPr/a:ln/a:prstDash/@val"l,
                    #idx:  ~x"./p:style/a:lnRef/@idx"l,
                    #couleurAccent: ~x"./p:style/a:lnRef/a:schemeClr/@val"l,
                    #shade: ~x"./p:style/a:lnRef/a:schemeClr/a:shade/@val"l,
                    #idxDeFillRef: ~x"./p:style/a:fillRef/@idx"l,
                    #CouleurFillRef: ~x"./p:style/a:fillRef/a:schemeClr/@val"l,
                    #idxEffectRef: ~x"./p:style/a:effectRef/@idx"l,
                    #CouleurEffectRef: ~x"./p:style/a:effectRef/a:schemeClr/@val"l,
                    #idxFontRef: ~x"./p:style/a:fontRef/@idx"l,
                    #CouleurFontRef: ~x"./p:style/a:fontRef/a:schemeClr/@val"l,
                     anchor: ~x"./p:txBody/a:bodyPr/@anchor[1]",
                     #rtlCol: ~x"./p:txBody/a:bodyPr/@rtlCol"l,
                     wrap: ~x"./p:txBody/a:bodyPr/@wrap[1]",
                     algn:  ~x"./p:txBody/a:p/a:pPr/@algn[1]",
                     langForme: ~x"./p:txBody/a:p/a:endParaRPr/@lang[1]",
                     langTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@lang[1]",
                     dirtyTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@dirty[1]",
                     uTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@u[1]",
                     boldTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@b"l,
                     szTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@sz"l,
                     couleurTexte1: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:srgbClr/@val"l,
                     couleurTexte2: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:schemeClr/@val"l,
                     shadeTexte: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:schemeClr/a:lumOff/@val"l,
                     contenuZoneDeTexte: ~x"./p:txBody/a:p/a:r/a:t/text()"l,
                     highlight: ~x"./p:txBody/a:p/a:r/a:rPr/a:highlight/a:srgbClr/@val[1]",
                     style: ~x"./p:txBody/a:p/a:r/a:rPr/a:latin/@typeface"l #|> SweetXml.transform_by(fn x -> Enum.map(x, fn elt -> if is_nil(elt) do 'Calibri' else elt end end) end)
             #                  , textFamily: 'Abadi'
             #                  , fontSize: 28
             #                  , fontWeight: 'bold'
             #                  , text: 'Salut'
             #                },
             #                %{},
             #                %{},
             #                %{}
             #              ]
             #
             ]
             )















             # result2 = Enum.map(result, fn {k, v} ->
             #   {k, List.first v} end )







             # result = doc |> xmap(
             #                Contenu: [
             #                  ~x"//p:cSld/p:spTree/p:sp"l,
             #                  forme: [
             #                    ~x"./p:nvSpPr",
             #                    name: ~x"./p:cNvPr/@name"
             #                  ],
             #                  spPr: [
             #                     ~x"./p:spPr"l,
             #                   xfrm:  [
             #                       ~x"./a:xfrm"l,
             #                     offy: ~x"./a:off/@y", #y
             #                     offx: ~x"./a:off/@x" ,#x
             #                     extcy: ~x"./a:ext/@cy" ,#cy
             #                     extcx: ~x"./a:ext/@cx", #cx
             #                     ],
             #                   solidFill: [
             #                       ~x"./a:solidFill"l,
             #                       schemeClr: ~x"./a:schemeClr/@val"
             #                   ],
             #                   ln: [
             #                     ~x"./a:ln"l,
             #                     w: ~x"./@w",
             #                     srgbClr: ~x"./a:solidFill/a:srgbClr/@val", #couleur des bordures
             #                     pointille: ~x"./a:prstDash/@val"
             #                   ]
             #
             #                  ],
             #                 style: [
             #                         ~x"./p:style"l,
             #
             #                     lnRef: [
             #                       ~x"./a:lnRef"l,
             #                     idx:  ~x"./@idx",
             #                     schemeClr: ~x"./a:schemeClr/@val"l,
             #                     shade: ~x"./a:schemeClr/a:shade/@val"l
             #                     ],
             #                     fillRef: [
             #                       ~x"./a:fillRef"l,
             #                     idx: ~x"./@idx"l,
             #                     schemeClr: ~x"./a:schemeClr/@val"l
             #                     ],
             #                     effectRef: [
             #                       ~x"./a:effectRef"l,
             #                     idx: ~x"./@idx"l,
             #                     schemeClr: ~x"./a:schemeClr/@val"l
             #                     ],
             #                     fontRef: [
             #                       ~x"./a:fontRef"l,
             #                     idx: ~x"./@idx"l,
             #                     schemeClr: ~x"./a:schemeClr/@val"l
             #                     ]
             #
             #                ],
             #                txBody: [
             #                      ~x"./p:txBody"l,
             #                  anchor: ~x"./a:bodyPr/@anchor"l,
             #                  rtlCol: ~x"./a:bodyPr/@rtlCol"l,
             #                  wrap: ~x"./a:bodyPr/@wrap"l,
             #                  algn:  ~x"./a:p/a:pPr/@algn"l,
             #                  langForme: ~x"./a:p/a:endParaRPr/@lang"l,
             #                  langTexte: ~x"./a:p/a:r/a:rPr/@lang"l,
             #                  dirtyTexte: ~x"./a:p/a:r/a:rPr/@dirty"l,
             #                  uTexte: ~x"./a:p/a:r/a:rPr/@u"l,
             #                  boldTexte: ~x"./a:p/a:r/a:rPr/@b"l,
             #                  szTexte: ~x"./a:p/a:r/a:rPr/@sz"l,
             #                  couleurTexte: ~x"./a:p/a:r/a:rPr/a:solidFill/a:srgbClr/@val"l,
             #                  contenuZoneDeTexte: ~x"./a:p/a:r/a:t/text()"l
             #
             #                ]
             #              ]
             #              )
             #






             # result =# doc |> xpath(~x"//matchup/name/text()")
             #    xpath(doc, ~x"//matchup/name/text()"l)
             #IO.inspect result
#     tmeta[wp:meta_key = '_thumbnail_id']/wp:meta_value/text()"s,
#         edited_at: ~x"./wp:postmeta[wp:meta_key = '_edit_last']/wp:meta_value/text()"s
#       )
# rows =
#       SweetXml.xpath(
#         tree,
#         ~x"/rss/channel/item"l,
#         slug: ~x"./wp:post_name/text()"s,
#         company_logo: ~x"./wp:pos
    #IO.inspect(rows)
  end
end
