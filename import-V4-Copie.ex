defmodule PrzXmlToMapWeb.Import do

  require Logger
  import SweetXml
  import XmlToMap

  def run(_args) do
    Mix.Task.run("app.start")
    import_dump()
  end

  def import_dump do


            doc = """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<p:sld xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="4" name="Rectangle 3"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{13AD18C6-1721-4F04-9E7D-34B8DD89DB89}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm rot="16200000"><a:off x="1252024" y="1350498"/><a:ext cx="3600000" cy="1800000"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:solidFill><a:schemeClr val="accent4"><a:lumMod val="60000"/><a:lumOff val="40000"/><a:alpha val="60000"/></a:schemeClr></a:solidFill><a:ln w="76200"><a:solidFill><a:srgbClr val="FF0000"/></a:solidFill></a:ln></p:spPr><p:style><a:lnRef idx="2"><a:schemeClr val="accent1"><a:shade val="50000"/></a:schemeClr></a:lnRef><a:fillRef idx="1"><a:schemeClr val="accent1"/></a:fillRef><a:effectRef idx="0"><a:schemeClr val="accent1"/></a:effectRef><a:fontRef idx="minor"><a:schemeClr val="lt1"/></a:fontRef></p:style><p:txBody><a:bodyPr rtlCol="0" anchor="ctr"/><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:endParaRPr lang="fr-FR"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Ellipse 4"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{290CF723-700B-4ACF-9968-130226CD4378}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm rot="600000"><a:off x="6096000" y="1055076"/><a:ext cx="3600000" cy="360000"/></a:xfrm><a:prstGeom prst="ellipse"><a:avLst/></a:prstGeom><a:solidFill><a:srgbClr val="FFFF00"><a:alpha val="80000"/></a:srgbClr></a:solidFill><a:ln><a:solidFill><a:srgbClr val="002060"/></a:solidFill></a:ln></p:spPr><p:style><a:lnRef idx="2"><a:schemeClr val="accent1"><a:shade val="50000"/></a:schemeClr></a:lnRef><a:fillRef idx="1"><a:schemeClr val="accent1"/></a:fillRef><a:effectRef idx="0"><a:schemeClr val="accent1"/></a:effectRef><a:fontRef idx="minor"><a:schemeClr val="lt1"/></a:fontRef></p:style><p:txBody><a:bodyPr rtlCol="0" anchor="ctr"/><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:endParaRPr lang="fr-FR"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="6" name="ZoneTexte 5"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{B5918D08-898B-4911-87B5-3AB7A43EAB84}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="6316394" y="3671668"/><a:ext cx="3108960" cy="707886"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="square" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:r><a:rPr lang="fr-FR" sz="4000" b="1" i="1" u="sng" dirty="0"><a:solidFill><a:schemeClr val="bg1"/></a:solidFill><a:highlight><a:srgbClr val="00FF00"/></a:highlight></a:rPr><a:t>Essai</a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="2" name="ZoneTexte 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{4B32F24B-83D3-415F-81C5-C41F2E4482B7}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="2588455" y="4937759"/><a:ext cx="4079631" cy="707886"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/></p:spPr><p:txBody><a:bodyPr wrap="square" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="fr-FR" sz="2800" b="1" i="1" dirty="0"/><a:t>Salut</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"><a:latin typeface="Abadi" panose="020B0604020104020204" pitchFamily="34" charset="0"/></a:rPr><a:t>Kenza</a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" sz="4000" dirty="0"/><a:t>ca</a:t></a:r><a:r><a:rPr lang="fr-FR" b="1" dirty="0"/><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t>va</a:t></a:r></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="4071009222"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>
                  """


             result = doc |> xmap(
               Contenu: [

                    ~x"//p:cSld/p:spTree/p:sp"l,
                    type: ~x"./p:nvSpPr/p:cNvPr/@name[1]"s |> SweetXml.transform_by(fn x -> hd(String.split(x)) end),
                    y: ~x"./p:spPr/a:xfrm/a:off/@y[1]",#y
                    x: ~x"./p:spPr/a:xfrm/a:off/@x[1]",#x
                    height:  ~x"./p:spPr/a:xfrm/a:ext/@cy[1]"s |> SweetXml.transform_by(fn x -> String.to_integer(x)/360000* 37.795275591 end) ,#cy #hauteur
                    width:  ~x"./p:spPr/a:xfrm/a:ext/@cx[1]"s |> SweetXml.transform_by(fn x -> String.to_integer(x)/360000* 37.795275591 end), #cx #largeur
                    ClrShade: ~x"./p:spPr/a:solidFill/a:schemeClr/a:lumOff/@val[1]"s |> SweetXml.transform_by(fn x -> if x=="" do x else String.to_integer(x)/1000 end end),
                    Couleur: ~x"./p:spPr/a:solidFill/a:schemeClr/@val[1]", #on récupére chaine de caractere
                    Couleur2: ~x"./p:spPr/a:solidFill/a:srgbClr/@val[1]", #on récupére un hexa
                    opacityShemaCLR: ~x"./p:spPr/a:solidFill/a:schemeClr/a:alpha/@val[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else ((100000 - String.to_integer(x)))/1000 end end),
                    opacitySrgbCLR: ~x"./p:spPr/a:solidFill/a:srgbClr/a:alpha/@val[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else ((100000 - String.to_integer(x)))/1000 end end),
                    CouleurBordure: ~x"./p:spPr/a:ln/a:solidFill/a:srgbClr/@val[1]", #couleur des bordures #verifiiiiiiiiiiier
                    Rotation: ~x"./p:spPr/a:xfrm/@rot[1]"s|> SweetXml.transform_by(fn x -> if x=="" do x else String.to_integer(x)/60000 end end), #En degré
                    #width: ~x"./p:spPr/a:ln/@w"l, #c pas la bonne taille
                    #pointille: ~x"./p:spPr/a:ln/a:prstDash/@val"l,
                    #idx:  ~x"./p:style/a:lnRef/@idx"l,
                    #couleurAccent: ~x"./p:style/a:lnRef/a:schemeClr/@val"l,
                    #shade: ~x"./p:style/a:lnRef/a:schemeClr/a:shade/@val"l,
                    #idxDeFillRef: ~x"./p:style/a:fillRef/@idx"l,
                    #CouleurFillRef: ~x"./p:style/a:fillRef/a:schemeClr/@val"l,
                    #idxEffectRef: ~x"./p:style/a:effectRef/@idx"l,
                    #CouleurEffectRef: ~x"./p:style/a:effectRef/a:schemeClr/@val"l,
                    #idxFontRef: ~x"./p:style/a:fontRef/@idx"l,
                    #CouleurFontRef: ~x"./p:style/a:fontRef/a:schemeClr/@val"l,
                     anchor: ~x"./p:txBody/a:bodyPr/@anchor[1]",
                     #rtlCol: ~x"./p:txBody/a:bodyPr/@rtlCol"l,
                     wrap: ~x"./p:txBody/a:bodyPr/@wrap[1]",
                     algn:  ~x"./p:txBody/a:p/a:pPr/@algn[1]",
                     langForme: ~x"./p:txBody/a:p/a:endParaRPr/@lang[1]",
                     langTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@lang[1]",
                     dirtyTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@dirty[1]",
                     uTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@u[1]",
                     boldTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@b"l,
                     szTexte: ~x"./p:txBody/a:p/a:r/a:rPr/@sz"l,
                     couleurTexte1: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:srgbClr/@val"l,
                     couleurTexte2: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:schemeClr/@val"l,
                     shadeTexte: ~x"./p:txBody/a:p/a:r/a:rPr/a:solidFill/a:schemeClr/a:lumOff/@val"l,
                     contenuZoneDeTexte: ~x"./p:txBody/a:p/a:r/a:t/text()"l,
                     highlight: ~x"./p:txBody/a:p/a:r/a:rPr/a:highlight/a:srgbClr/@val[1]",
                     Style: ~x"./p:txBody/a:p/a:r/a:rPr/a:latin/@typeface"osl, #|> SweetXml.transform_by(fn x -> Enum.map(x, fn elt -> if is_nil(elt) do 'Calibri' else elt end end) end)
                                         #          , textFamily: 'Abadi'
             #                  , fontSize: 28
             #                  , fontWeight: 'bold'
             #                  , text: 'Salut'
             #                },
             #                %{},
             # "ItemDimensions": { #data["ItemDimensions"]["color"]["#content"] = "0.5"
             #     "color": {
             #           "#content": "0.50",
             #           "-Units": "inches"
             #     },
             #     "a:srgbClr": {
             #       "a:alpha": {
             #         "#content": "",
             #         "-val": "80000"
             #       }
             #       {
             #         "#content": "",
             #         "-val": "FFF00"
             #       }
             #       #Il faut faire tout le chemin jusqu'au p:spPr/a:solidFill/a:schemeClr/a:lumOff["-val"] il est là
             #
             #       }
             #     }
             ]
             )

  end


  def colorConversion(color, shadexml) do
    shade = to_string (String.to_integer(shadexml) / 1000)
    case color do
      "bg1" ->
        case shade do
          "65" -> "#a5a5a5"
          "75" -> "#bfbfbf"
          "50" -> "#7f7f7f"
          "85" -> "#d8d8d8"
          "95" -> "#f2f2f2"
          _ -> "#ffffff" #cas par défaut
        end

      "tx1" ->
        case shade do
          "85" -> "#262626"
          "75" -> "#3f3f3f"
          "95" -> "#0c0c0c"
          "65" -> "#595959"
          "50" -> "#7f7f7f"
          _ -> "#000000" #cas par défaut
        end

      "bg2" ->
        case shade do
          "25" -> "#3a3838"
          "50" -> "#757070"
          "10" -> "#171616"
          "75" -> "#aeabab"
          "90" -> "#d0cece"
          _ -> "#e7e6e6" #cas par défaut
        end

      "tx2" ->
        case shade do
          "75" -> "#323f4f"
          "60" -> "#8496b0"
          "50" -> "#222a35"
          "40" -> "#adb9ca"
          "20" -> "#d6dce4"
          _ -> "#44546a" #cas par défauts
        end
        "accent1" ->
          case shade do
            "75" -> "#2f5496"
            "60" -> "#8eaadb"
            "50" -> "#1f3864"
            "40" -> "#b4c6e7"
            "20" -> "#d9e2f3"
            _ -> "#4472c4" #cas par défaut
          end

        "accent2" ->
          case shade do
            "75" -> "#c55a11"
            "60" -> "#f4b183"
            "50" -> "#833c0b"
            "40" -> "#f7cbac"
            "20" -> "#fbe5d5"
               _ ->  "#ed7d31"  #cas par défaut
         end

       "accent3" ->
         case shade do
           "75" -> "#7b7b7b"
           "60" -> "#c9c9c9"
           "50" -> "#525252"
           "40" -> "#dbdbdb"
           "20" -> "#ededed"
           _ -> "#a5a5a5" #cas par défaut
         end

       "accent4" ->
         case shade do
           "75" -> "#bf9000"
           "60" -> "#ffd965"
           "50" -> "#7f6000"
           "40" -> "#fee599"
           "20" -> "#fff2cc"
           _ -> "#ffc000" #cas par défaut
         end

       "accent5" ->
         case shade do
           "75" -> "#2e75b5"
           "60" -> "#9cc3e5"
           "50" -> "#1e4e79"
           "40" -> "#bdd7ee"
           "20" -> "#deebf6"
           _ -> "#5b9bd5" #cas par défaut
         end

       "accent6" ->
         case shade do
           "75" -> "#538135"
           "60" -> "#a8d08d"
           "50" -> "#375623"
           "40" -> "#c5e0b3"
           "20" -> "#e2efd9"
           _ -> "#70ad47" #cas par défaut
         end
       #_->
    end
  end


  def recuperation() do

    data = ~S(<?xml version="1.0" encoding="UTF-8" standalone="yes"?><p:sld xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="3" name="Sous-titre 2"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{D20DD0B6-DBBE-4D15-A7D1-D55B43F872B7}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="subTitle" idx="1"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:pPr algn="l"/><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg1"><a:lumMod val="50000"/></a:schemeClr></a:solidFill></a:rPr><a:t>Un</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="90000"/></a:schemeClr></a:solidFill></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg1"><a:lumMod val="65000"/></a:schemeClr></a:solidFill><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t>deux</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="90000"/></a:schemeClr></a:solidFill></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="95000"/><a:lumOff val="5000"/></a:schemeClr></a:solidFill></a:rPr><a:t>trois</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="90000"/></a:schemeClr></a:solidFill><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="tx1"><a:lumMod val="85000"/><a:lumOff val="15000"/></a:schemeClr></a:solidFill></a:rPr><a:t>quatre</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:srgbClr val="FF0000"/></a:solidFill></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="10000"/></a:schemeClr></a:solidFill></a:rPr><a:t>cinq </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="25000"/></a:schemeClr></a:solidFill><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t>six</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:srgbClr val="FF0000"/></a:solidFill></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="tx2"><a:lumMod val="50000"/></a:schemeClr></a:solidFill></a:rPr><a:t>sept</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="bg2"><a:lumMod val="90000"/></a:schemeClr></a:solidFill><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="tx2"><a:lumMod val="75000"/></a:schemeClr></a:solidFill></a:rPr><a:t>huit </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="accent1"><a:lumMod val="50000"/></a:schemeClr></a:solidFill></a:rPr><a:t>neuf</a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="tx2"><a:lumMod val="75000"/></a:schemeClr></a:solidFill><a:latin typeface="Algerian" panose="04020705040A02060702" pitchFamily="82" charset="0"/></a:rPr><a:t> </a:t></a:r><a:r><a:rPr lang="fr-FR" dirty="0"><a:solidFill><a:schemeClr val="accent1"><a:lumMod val="75000"/></a:schemeClr></a:solidFill></a:rPr><a:t>dix </a:t></a:r></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="2" name="Ellipse 1"><a:extLst><a:ext uri="{FF2B5EF4-FFF2-40B4-BE49-F238E27FC236}"><a16:creationId xmlns:a16="http://schemas.microsoft.com/office/drawing/2014/main" id="{496655BC-540A-4105-BAD4-5ED20B12B4E6}"/></a:ext></a:extLst></p:cNvPr><p:cNvSpPr/><p:nvPr/></p:nvSpPr><p:spPr><a:xfrm><a:off x="1323474" y="1010653"/><a:ext cx="2334126" cy="1419726"/></a:xfrm><a:prstGeom prst="ellipse"><a:avLst/></a:prstGeom><a:solidFill><a:srgbClr val="FF0000"><a:alpha val="50000"/></a:srgbClr></a:solidFill></p:spPr><p:style><a:lnRef idx="2"><a:schemeClr val="accent1"><a:shade val="50000"/></a:schemeClr></a:lnRef><a:fillRef idx="1"><a:schemeClr val="accent1"/></a:fillRef><a:effectRef idx="0"><a:schemeClr val="accent1"/></a:effectRef><a:fontRef idx="minor"><a:schemeClr val="lt1"/></a:fontRef></p:style><p:txBody><a:bodyPr rtlCol="0" anchor="ctr"/><a:lstStyle/><a:p><a:pPr algn="ctr"/><a:endParaRPr lang="fr-FR"><a:solidFill><a:schemeClr val="bg1"><a:lumMod val="50000"/></a:schemeClr></a:solidFill></a:endParaRPr></a:p></p:txBody></p:sp></p:spTree><p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="2545792501"/></p:ext></p:extLst></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld>)
    productMap =
      XmlToMap.naive_map(data)
    p = "{http://schemas.openxmlformats.org/presentationml/2006/main}"
    a = "{http://schemas.openxmlformats.org/drawingml/2006/main}"

    listOfSp = productMap["#{p}sld"]["#{p}cSld"]["#{p}spTree"]["#{p}sp"]
    listOfElement = Enum.map(listOfSp, fn every_sp ->

      name = every_sp["#{p}nvSpPr"]["#{p}cNvPr"]["-name"]
      xString = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}off"]["-x"] #6096000
      yString = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}off"]["-y"] #1055076
      rtString = every_sp["#{p}spPr"]["#{a}xfrm"]["-rot"] #16200000
      widthString = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}ext"]["-cx"] #3600000 String.to_integer(x)/360000* 37.795275591
      heightString = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}ext"]["-cy"] #360000

      Logger.warn "Name #{inspect name}"




      IO.inspect name
        [head | tail] =
          if not is_nil(name) do
            String.split(name)
          else
            ["unknown_type"]
          end
        IO.inspect head
        shapeType =
          case head do
            "Ellipse" -> "circle"
            "Rectangle" -> "rectangle"
            "ZoneTexte" -> "text"
            "Sous-titre" -> "text"
            "I.." -> "image"
            "V.." -> "video"
            _ -> head
          end

        # Units conversions
        width = toPrzDimension(widthString)
        height = toPrzDimension(heightString)
        x = toPrzDimension(xString)
        y = toPrzDimension(yString)
        rt = toPrzRt(rtString)

        Logger.debug "WIDTH#{inspect width}"
        Logger.debug "HEIGHT#{inspect height}"
        Logger.debug "RT#{inspect rt}"
        Logger.debug "X#{inspect x}"
        Logger.debug "Y#{inspect y}"

        result =
          case shapeType do
            "text" ->
                  IO.inspect "text shape"
                  listOfWords = every_sp["#{p}txBody"]["#{a}p"]["#{a}r"]


                  list_contenu =
                      Enum.map(listOfWords, fn word ->
                        #Logger.debug "word#{inspect word}"
                        #####
                        #IO.inspect "entreeeeeee"
                        #IO.inspect word["#{a}rPr"]
                        solidfill = word["#{a}rPr"]["#content"]["#{a}solidFill"]
                        spanStrColor = solidfill["#{a}schemeClr"]
                        spanShadeString = spanStrColor["#content"]["#{a}lumMod"]
                        spanHexaColor = solidfill["#{a}srgbClr"]

                        #Logger.debug "solidfill#{inspect solidfill}"
                        Logger.debug "spanStrColor#{inspect spanStrColor}"
                        Logger.debug "spanShadeString#{inspect spanShadeString}"
                        Logger.debug "spanHexaColor #{inspect spanHexaColor}"

                        if not is_nil(spanHexaColor) do # Autres couleurs
                          %{ color: spanHexaColor["-val"],
                             opacity: 1,

                           }
                        else # palettes
                          palresult =
                            if is_nil(spanShadeString) do
                              %{ color: spanStrColor["-val"],
                                 opacity: "100000"
                               }
                            else
                              %{ color: spanStrColor["-val"],
                                 opacity: spanShadeString["-val"]
                               }
                            end
                          %{ color: colorConversion(palresult.color, palresult.opacity ),
                              opacity: 1 }
                        end




                        b = word["#{a}rPr"]["-b"]
                        font_weight =
                          case b do
                            "1" -> "bold"
                            _ -> "normal"
                          end

                        i = word["#{a}rPr"]["-i"]
                        font_style =
                          case i do
                            "1" -> "italic"
                            _ -> "normal"
                          end

                        sng = word["#{a}rPr"]["-u"]
                        text_decoration =
                          case sng do
                            "sng" -> "underline"
                            _ -> "none"
                          end

                        #IO.inspect "siiiiiiize"
                        innerText = word["#{a}t"]
                        #IO.inspect innerText
                        spanSize = word["#{a}rPr"]["-sz"]
                        spanPolice = word["#{a}rPr"]["#content"]["#{a}latin"]["-typeface"]
                        #Logger.debug "#{inspect spanPolice}"
                        font_family= if is_nil(spanPolice) do "Calibri" else spanPolice end
                        Logger.debug "#{inspect font_family}"
                        font_size = toPrzFontSize(spanSize)
                        #Logger.debug "#{inspect spanSize2}"
                        spanTxt2 =
                          if innerText == " " || is_nil(innerText) || innerText == %{} do
                            "&nbsp;"
                          else
                            "<span style='font-weight: #{font_weight};font-style: #{font_style};text-decoration: #{text_decoration};font-size: #{font_size};font-family: #{font_family};'>" <> innerText <>"</span>"
                          end
                        Logger.error "#{inspect spanTxt2}"
                        spanTxt2
                      end)
                      contenu = Enum.join(list_contenu, "")
                  IO.inspect contenu
                  lastContent = ""#{}"<p>" <> contenu <> "</p>"

                  %{ version: 1,
                     type: shapeType,
                     content: lastContent, #{}"<p><span>un</span> <span>deux</span> trois quatre 8 </p>",
                     y: x,
                     x: y,
                     width: width,
                     height: height,
                     translateMatrix: %{ m32: 0, m31: 0, m23: 0, m22: 1, m21: 0, m13: 0, m12: 0, m11: 1},
                     rt: 0,
                     rotateMatrix: %{ m32: 0, m31: 0, m23: 0, m22: 1, m21: 0, m13: 0,m12: 0, m11: 1},
                     parent_bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 },
                     bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 }
                   }

            _ -> # Default case
                  strokeWidth = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}ln"]["-w"] #76200
                  stroke = every_sp["#{p}spPr"]["#{a}xfrm"]["#{a}ln"]["#{a}solidFill"]["#{a}srgbClr"]["-val"]
                  #Il faut déclarer une variable Opacity qui aura la valeur de alpha
                  Logger.debug "Stroke #{inspect stroke}"
                  Logger.debug "StrokeWidth #{inspect strokeWidth}"

                  hexaColor = every_sp["#{p}spPr"]["#{a}solidFill"]["#{a}srgbClr"]
                  paletteColor = every_sp["#{p}spPr"]["#{a}solidFill"]["#{a}schemeClr"]
                  Logger.debug "hexaColor #{inspect hexaColor}"
                  Logger.debug "SchemeColor #{inspect paletteColor}"
                  [color, shade] =
                    if not is_nil(paletteColor) do # Couleurs depuis les colonnes
                      paletteShade = every_sp["#{p}spPr"]["#{a}solidFill"]["#{a}schemeClr"]["#{a}lumMod"]

                      [clr, sha ] = # Sub-case on shade
                        if is_nil(paletteShade) do
                          [ paletteColor["-val"], "100000" ]
                        else
                          [paletteColor["-val"], paletteShade["-val"] ] #ici on retourne un shade de couleur alors qu'en bas on retourne l'opacity
                        end
                      [ colorConversion(clr, sha), 1 ]

                    else # Autres couleurs
                      hexaShade =
                        every_sp["#{p}spPr"]["#{a}solidFill"]["#{a}srgbClr"]["#{a}alpha"] #transparence != shade
                      if is_nil(hexaShade) do
                        [ hexaColor["-val"], 1 ]
                      else
                        [hexaColor["-val"], toPrzOpacity(hexaShade["-val"]) ] #hexashade ne doit pas exister car le shade est tjrs a 1 quand c en hexa
                      end
                    end
                    %{ version: 1,
                       type: shapeType,
                       strokeWidth: strokeWidth,
                       stroke: stroke,
                       ry: 1,
                       rx: 1,
                       fillOpacity: shade,
                       fill: color,
                       y: x,
                       x: y,
                       width: width,
                       height: height,
                       translateMatrix: %{ m32: 0, m31: 0, m23: 0, m22: 1, m21: 0, m13: 0, m12: 0, m11: 1},
                       rt: 0,
                       rotateMatrix: %{ m32: 0, m31: 0, m23: 0, m22: 1, m21: 0, m13: 0,m12: 0, m11: 1},
                       parent_bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 },
                       bbox: %{ b_y: 0, b_x: 0, b_width: 0, b_height: 0 }
                     }

          end

      end)
  end

  defp toPrzOpacity(transparence) do
    1 - ((String.to_integer transparence) / 100000)
  end

  defp toPrzDimension(widthString) do
    if is_nil(widthString) do
      0
    else
      (String.to_integer(widthString)/360000) * 37.795275591
    end
  end

  defp toPrzRt(rtString) do

    if is_nil(rtString) do
       0
    else
      String.to_float(rtString)/60000
    end
  end

  defp toPrzFontSize(spanSize) do
    if is_nil(spanSize) do
      "18px"
    else
      (to_string (String.to_integer(spanSize)/100)) <> "px"
    end
  end




end
