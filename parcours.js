const path = require('path')
const glob = require('glob');
const fs = require('fs');
const xml2js = require('xml2js');

$(document).ready(function() {
    var files = glob.sync(path.join(__dirname, '../db/*/*.xml'));
    var counter = 0;
    files.forEach(function(f) {
        fs.readFile(f, function(err, data) {
            var parser = new xml2js.Parser({explicitArray : false});
            parser.parseString(data, function(err, xml) {
                // parsing done here
            });
            counter++;
            if (counter === files.length) {
                $(document).trigger('completed-xml-db-parse');
            }
        });
    });
});